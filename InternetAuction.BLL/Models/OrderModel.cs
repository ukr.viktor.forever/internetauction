﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetAuction.BLL.Models
{
    public class OrderModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int OrderTypeId { get; set; }
        public bool IsConfirmed { get; set; }
        public int ProductId { get; set; }
    }

}
