﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetAuction.BLL.Models
{
    public class ProductCategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<int> ProductsId { get; set; }
    }
}
