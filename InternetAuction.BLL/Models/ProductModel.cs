﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetAuction.BLL.Models
{
    public class ProductModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int PictureId { get; set; }
        public DateTime PublicationDate { get; set; }
        public DateTime ClosingDate { get; set; }
        public int PriceId { get; set; }
        public int CategoryId { get; set; }
        public ICollection<int> CommentsIds { get; set; }
        public int AuthorId { get; set; }
    }
}
