﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetAuction.BLL.Models
{
    public class CommentModel
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public int ProductId { get; set; }
        public ICollection<int> ChildCommentsIds { get; set; }
    }
}
