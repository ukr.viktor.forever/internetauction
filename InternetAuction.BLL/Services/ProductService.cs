﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using InternetAuction.BLL.Interfaces;
using InternetAuction.BLL.Models;
using InternetAuction.DAL.Interfaces;
using InternetAuction.DAL.Entities;
using System.Globalization;

namespace InternetAuction.BLL.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork db;
        private readonly IMapper mapper;
        public ProductService(IUnitOfWork repository, IMapper mapper)
        {
            db = repository;
            this.mapper = mapper;
        }
        public async Task<IEnumerable<ProductModel>> GetAllProducts()
        {
            IEnumerable<Product> products = await db.Products.GetAllWithDetails();
            List<ProductModel> prodModels = new List<ProductModel>();

            foreach (var game in products)
            {
                prodModels.Add(mapper.Map<ProductModel>(game));
            }
            return prodModels;
        }
        public async Task<ProductModel> GetProductByIdAsync(int id)
        {
            Product product = await db.Products.GetByIdWithDetails(id);
            return mapper.Map<ProductModel>(product);
        }
        public async Task<IEnumerable<ProductModel>> GetProductsByCategory(string gameGenreName)
        {
            IEnumerable<Product> products = await db.Products.GetProductByCategory(gameGenreName);
            List<ProductModel> prodModels = new List<ProductModel>();

            foreach (var product in products)
            {
                prodModels.Add(mapper.Map<ProductModel>(product));
            }
            return prodModels;
        }
        public async Task<IEnumerable<ProductModel>> GetProductsByAuthorId(int id)
        {
            IEnumerable<Product> products = await db.Products.GetProductsByAuthorId(id);
            List<ProductModel> prodModels = new List<ProductModel>();

            foreach (var product in products)
            {
                prodModels.Add(mapper.Map<ProductModel>(product));
            }
            return prodModels;
        }
        public async Task<IEnumerable<ProductModel>> GetProductsByAuthorName(string authorName)
        {
            var products = await db.Products.GetProductsByAuthorName(authorName);
            List<ProductModel> models = new List<ProductModel>();

            foreach (var product in products)
            {
                models.Add(mapper.Map<ProductModel>(product));
            }
            return models;
        }
        public async Task<IEnumerable<CommentModel>> GetComentsByProductId(int id)
        {
            Product product = await db.Products.GetByIdWithDetails(id);
            ICollection<Comment> comments = product.Comments;
            List<CommentModel> commentModels = new List<CommentModel>();

            foreach (var comment in comments)
            {
                commentModels.Add(mapper.Map<CommentModel>(comment));
            }
            return commentModels;
        }
        public async Task<IEnumerable<ProductModel>> GetProductsByPublicationDate(string date)
        {
            DateTime dateTime;
            if (DateTime.TryParse(date, out dateTime))
            {
                IEnumerable<Product> products = await db.Products.GetProductsByPublicationData(dateTime);
                List<ProductModel> models = new List<ProductModel>();

                foreach (var product in products)
                {
                    models.Add(mapper.Map<ProductModel>(product));
                }
                return models;
            }
            else return null;
        }
        public async Task<IEnumerable<ProductModel>> GetProductsByClosingData(string date)
        {
            DateTime dateTime;
            if (DateTime.TryParse(date, out dateTime))
            {
                IEnumerable<Product> products = await db.Products.GetProductsByClosingData(dateTime);
                List<ProductModel> models = new List<ProductModel>();

                foreach (var product in products)
                {
                    models.Add(mapper.Map<ProductModel>(product));
                }
                return models;
            }
            else return null;
        }
        public async Task AddProductAsync(ProductModel model)
        {
            Product product = mapper.Map<Product>(model);
            await db.Products.AddAsync(product);
        }
        public async Task DeleteProductByIdAsync(int id)
        {
            await db.Products.DeleteByIdAsync(id);
        }
        public async Task UpdateProductAsync(ProductModel model)
        {
            Product product = mapper.Map<Product>(model);
            await db.Products.Update(product);
        }
    }
}
