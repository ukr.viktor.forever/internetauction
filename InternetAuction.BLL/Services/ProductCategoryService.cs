﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using InternetAuction.BLL.Interfaces;
using InternetAuction.BLL.Models;
using InternetAuction.DAL.Interfaces;
using InternetAuction.DAL.Entities;

namespace InternetAuction.BLL.Services
{
    public class ProductCategoryService : IProductCategoryService
    {
        private readonly IUnitOfWork db;
        private readonly IMapper mapper;
        public ProductCategoryService(IUnitOfWork repository, IMapper mapper)
        {
            db = repository;
            this.mapper = mapper;
        }
        public async Task<IEnumerable<ProductCategoryModel>> GetAllCategories()
        {
            IEnumerable<ProductCategory> categories = await db.Categories.GetAllWithDetails();
            List<ProductCategoryModel> categoryModel = new List<ProductCategoryModel>();
            foreach (var category in categories)
            {
                categoryModel.Add(mapper.Map<ProductCategoryModel>(category));
            }
            return categoryModel;
        }
        public async Task<ProductCategoryModel> GetCategoryByIdAsync(int id)
        {
            ProductCategory category = await db.Categories.GetByIdWithDetails(id);
            return mapper.Map<ProductCategoryModel>(category);
        }

        public async Task AddCategoryAsync(ProductCategoryModel model)
        {
            ProductCategory category = mapper.Map<ProductCategory>(model);
            await db.Categories.AddAsync(category);
        }
        public async Task UpdateCategoryAsync(ProductCategoryModel model)
        {
            ProductCategory entity = mapper.Map<ProductCategory>(model);
            await db.Categories.Update(entity);
        }
        public async Task DeleteCategoryByIdAsync(int id)
        {
            await db.Categories.DeleteByIdAsync(id);
        }
    }
}
