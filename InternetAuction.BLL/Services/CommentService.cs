﻿using InternetAuction.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using InternetAuction.DAL;
using AutoMapper;
using System.Threading.Tasks;
using InternetAuction.BLL.Models;
using InternetAuction.DAL.Entities;
using InternetAuction.DAL.Interfaces;

namespace InternetAuction.BLL.Services
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork db;
        private readonly IMapper mapper;
        public CommentService(IUnitOfWork repository, IMapper mapper)
        {
            db = repository;
            this.mapper = mapper;
        }
        public async Task AddCommentAsync(CommentModel model)
        {
            Comment comment = mapper.Map<Comment>(model);
            await db.Comments.AddAsync(comment);
        }
        public async Task DeleteCommentByIdAsync(int id)
        {
            await db.Comments.DeleteByIdAsync(id);
        }
        public async Task<IEnumerable<CommentModel>> GetAllComment()
        {
            IEnumerable<Comment> comments = await db.Comments.GetAllWithDetails();
            List<CommentModel> models = new List<CommentModel>();

            foreach (var comment in comments)
            {
                models.Add(mapper.Map<CommentModel>(comment));
            }
            return models;
        }
        public async Task<CommentModel> GetCommentByIdAsync(int id)
        {
            Comment comment = await db.Comments.GetByIdWithDetails(id);
            CommentModel model = mapper.Map<CommentModel>(comment);
            return model;
        }
        public async Task UpdateCommentAsync(CommentModel model)
        {
            Comment comment = mapper.Map<Comment>(model);
            await db.Comments.Update(comment);
        }
    }
}
