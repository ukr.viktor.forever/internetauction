﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InternetAuction.BLL.Models;

namespace InternetAuction.BLL.Interfaces
{
    public interface IProductCategoryService
    {
        Task<IEnumerable<ProductCategoryModel>> GetAllCategories();
        Task<ProductCategoryModel> GetCategoryByIdAsync(int id);
        Task AddCategoryAsync(ProductCategoryModel model);
        Task UpdateCategoryAsync(ProductCategoryModel model);
        Task DeleteCategoryByIdAsync(int id);
    }
}
