﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InternetAuction.BLL.Models;

namespace InternetAuction.BLL.Interfaces
{
    public interface ICommentService
    {
        Task<IEnumerable<CommentModel>> GetAllComment();
        Task<CommentModel> GetCommentByIdAsync(int id);
        Task AddCommentAsync(CommentModel model);
        Task UpdateCommentAsync(CommentModel model);
        Task DeleteCommentByIdAsync(int id);
    }
}
