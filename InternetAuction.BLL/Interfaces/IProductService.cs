﻿using InternetAuction.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InternetAuction.BLL.Interfaces
{
    public interface IProductService
    {
        Task<IEnumerable<ProductModel>> GetAllProducts();
        Task<ProductModel> GetProductByIdAsync(int id);
        Task<IEnumerable<ProductModel>> GetProductsByCategory(string gameGenreName);
        Task<IEnumerable<ProductModel>> GetProductsByAuthorId(int id);
        Task<IEnumerable<ProductModel>> GetProductsByPublicationDate(string date);
        Task<IEnumerable<ProductModel>> GetProductsByClosingData(string date);
        Task<IEnumerable<ProductModel>> GetProductsByAuthorName(string authorName);
        Task AddProductAsync(ProductModel model);
        Task UpdateProductAsync(ProductModel model);
        Task DeleteProductByIdAsync(int id);

        Task<IEnumerable<CommentModel>> GetComentsByProductId(int id);
    }
}
