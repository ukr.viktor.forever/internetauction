﻿using AutoMapper;
using InternetAuction.BLL.Models;
using InternetAuction.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternetAuction.API
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserRegisterModel, AppUser>();


            CreateMap<Comment, CommentModel>()
                .ForMember(u => u.ProductId, opt => opt.MapFrom(ur => ur.Product.Id))
                .ForMember(u => u.ChildCommentsIds, opt => opt.MapFrom(ur => ur.ChildComments.Select(x => x.Id)))
                .ReverseMap();

            CreateMap<Product, ProductModel>()
                .ForMember(u => u.PictureId, opt => opt.MapFrom(ur => ur.Pictures.Select(x => x.Id)))
                .ForMember(u => u.PictureId, opt => opt.MapFrom(ur => ur.Price.Id))
                .ForMember(u => u.CategoryId, opt => opt.MapFrom(ur => ur.Category.Id))
                .ForMember(u => u.CommentsIds, opt => opt.MapFrom(ur => ur.Comments.Select(x => x.Id)))
                .ReverseMap();

            CreateMap<Order, OrderModel>()
                .ForMember(u => u.UserId, opt => opt.MapFrom(ur => ur.User.Id))
                .ForMember(u => u.OrderTypeId, opt => opt.MapFrom(ur => ur.OrderType.Id))
                .ForMember(u => u.ProductId, opt => opt.MapFrom(ur => ur.Product.Id))
                .ReverseMap();

            CreateMap<ProductCategory, ProductCategoryModel>()
                .ForMember(u => u.ProductsId, opt => opt.MapFrom(ur => ur.Products.Select(x=>x.Id)))
                .ReverseMap();

        }
    }
}
