﻿using InternetAuction.BLL.Interfaces;
using InternetAuction.BLL.Models;
using InternetAuction.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternetAuction.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : Controller
    {
        private readonly ICommentService service;
        private readonly UserManager<AppUser> userManager;

        public CommentController(ICommentService service, UserManager<AppUser> userManager)
        {
            this.service = service;
            this.userManager = userManager;
        }
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<CommentModel>>> GetAllComment()
        {
            return Ok(await service.GetAllComment());
        }
        [HttpGet("id")]
        public async Task<ActionResult<CommentModel>> GetCommentById(int id)
        {
            return Ok(await service.GetCommentByIdAsync(id));
        }

        [Authorize]
        [HttpPost("add")]
        public async Task<IActionResult> AddComment(CommentModel model)
        {
            var author = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var authorId = author.Id;
            model.AuthorId = authorId;
            await service.AddCommentAsync(model);
            return Ok();
        }

        [Authorize]
        [HttpPut("update")]
        public async Task<IActionResult> UpdateComment(CommentModel model)
        {
            var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var userRoles = await userManager.GetRolesAsync(user);

            var comment = await service.GetCommentByIdAsync(model.Id);

            if (userRoles.Contains("Admin") || comment.AuthorId == user.Id)
            {
                await service.UpdateCommentAsync(model);
                return Ok();
            }
            else
            {
                return BadRequest("Not permitted");
            }
        }

        [Authorize]
        [HttpDelete("delete")]
        public async Task<IActionResult> DeleteComment(int id)
        {
            var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var userRoles = await userManager.GetRolesAsync(user);

            var comment = await service.GetCommentByIdAsync(id);

            if (userRoles.Contains("Admin") || comment.AuthorId == user.Id)
            {
                await service.DeleteCommentByIdAsync(id);
                return Ok();
            }
            else
            {
                return BadRequest("Not permitted");
            }
        }
    }
}
