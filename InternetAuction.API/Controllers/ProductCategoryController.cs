﻿using InternetAuction.BLL.Interfaces;
using InternetAuction.BLL.Models;
using InternetAuction.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternetAuction.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductCategoryController : Controller
    {
        private readonly IProductCategoryService service;
        private readonly UserManager<AppUser> userManager;

        public ProductCategoryController(IProductCategoryService service, UserManager<AppUser> userManager)
        {
            this.service = service;
            this.userManager = userManager;
        }
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<ProductCategoryModel>>> GetAllCategories()
        {
            return Ok(await service.GetAllCategories());
        }
        [HttpGet("id")]
        public async Task<ActionResult<ProductCategoryModel>> GetCategoryByIdAsync(int id)
        {
            return Ok(await service.GetCategoryByIdAsync(id));
        }


        [Authorize(Roles = "Admin")]
        [HttpPost("add")]
        public async Task<IActionResult> AddCategory(ProductCategoryModel model)
        {
            await service.AddCategoryAsync(model);
            return Ok();
        }
        [Authorize(Roles = "Admin")]
        [HttpPut("update")]
        public async Task<IActionResult> UpdateProduct(ProductCategoryModel model)
        {
            await service.UpdateCategoryAsync(model);
            return Ok();
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("delete")]
        public async Task<IActionResult> DeleteCategoryById(int id)
        {
            await service.DeleteCategoryByIdAsync(id);
            return Ok();
        }
    }
}
