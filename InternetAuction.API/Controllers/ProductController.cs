﻿using InternetAuction.BLL.Interfaces;
using InternetAuction.BLL.Models;
using InternetAuction.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternetAuction.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly IProductService service;
        private readonly UserManager<AppUser> userManager;

        public ProductController(IProductService service, UserManager<AppUser> userManager)
        {
            this.service = service;
            this.userManager = userManager;
        }
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<ProductModel>>> GetAllProducts()
        {
            return Ok(await service.GetAllProducts());
        }
        [HttpGet("id")]
        public async Task<ActionResult<ProductModel>> GetProductById(int id)
        {
            return Ok(await service.GetProductByIdAsync(id));
        }
        [HttpGet("category")]
        public async Task<ActionResult<IEnumerable<ProductModel>>> GetProductsByCategory(string category)
        {
            return Ok(await service.GetProductsByCategory(category));
        }
        [HttpGet("authorId")]
        public async Task<ActionResult<IEnumerable<ProductModel>>> GetProductsByAuthorId(int id)
        {
            return Ok(await service.GetProductsByAuthorId(id));
        }
        [HttpGet("authorName")]
        public async Task<ActionResult<IEnumerable<ProductModel>>> GetProductsByAuthorName(string authorName)
        {
            return Ok(await service.GetProductsByAuthorName(authorName));
        }
        [HttpGet("commentId")]
        public async Task<ActionResult<IEnumerable<CommentModel>>> GetComentsByProductId(int id)
        {
            return Ok(await service.GetComentsByProductId(id));
        }

        [HttpGet("publicationDate")]
        public async Task<ActionResult<IEnumerable<CommentModel>>> GetProductsByPublicationDate(string date)
        {
            return Ok(await service.GetProductsByPublicationDate(date));
        }

        [HttpGet("closingDate")]
        public async Task<ActionResult<IEnumerable<CommentModel>>> GetProductsByClosingData(string date)
        {
            return Ok(await service.GetProductsByClosingData(date));
        }


        [Authorize]
        [HttpPost("add")]
        public async Task<IActionResult> AddProduct(ProductModel model)
        {
            var author = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var authorId = author.Id;
            model.AuthorId = authorId;
            model.PublicationDate = DateTime.Now;
            await service.AddProductAsync(model);
            return Ok();
        }
        [Authorize]
        [HttpPut("update")]
        public async Task<IActionResult> UpdateProduct(ProductModel model)
        {
            var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var userRoles = await userManager.GetRolesAsync(user);

            var product = await service.GetProductByIdAsync(model.Id);

            if (userRoles.Contains("Admin") || product.AuthorId == user.Id)
            {
                await service.UpdateProductAsync(model);
                return Ok();
            }
            else
            {
                return BadRequest("Not permitted");
            }
        }

        [Authorize]
        [HttpDelete("delete")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var userRoles = await userManager.GetRolesAsync(user);

            var product = await service.GetProductByIdAsync(id);

            if (userRoles.Contains("Admin") || product.AuthorId == user.Id)
            {
                await service.DeleteProductByIdAsync(id);
                return Ok();
            }
            else
            {
                return BadRequest("Not permitted");
            }
        }
    }
}
