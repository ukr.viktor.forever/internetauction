﻿using AutoMapper;
using InternetAuction.API.Settings;
using InternetAuction.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using InternetAuction.BLL.Models;

namespace InternetAuction.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : Controller
    {
        private readonly UserManager<AppUser> userManager;
        private readonly JwtSettings jwtSettings;
        private readonly IMapper mapper;


        public LoginController(UserManager<AppUser> userManager, IOptionsSnapshot<JwtSettings> jwtSettings, IMapper mapper)
        {
            this.userManager = userManager;
            this.jwtSettings = jwtSettings.Value;
            this.mapper = mapper;
        }
        [HttpPost("SignIn")]
        public async Task<IActionResult> SignIn(UserLoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = userManager.Users.SingleOrDefault(u => u.UserName == model.UserName);
            if (user is null)
            {
                return NotFound("User not found");
            }

            var userSigninResult = await userManager.CheckPasswordAsync(user, model.Password);
            if (userSigninResult)
            {
                var roles = await userManager.GetRolesAsync(user);
                return Ok(new { accessToken = GenerateJwt(user, roles) });
            }
            return BadRequest("UserName or password incorrect.");
        }
        [HttpPost("Registration")]
        public async Task<IActionResult> Registration(UserRegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = mapper.Map<UserRegisterModel, AppUser>(model);
            var userCreateResult = await userManager.CreateAsync(user, model.Password);

            if (userCreateResult.Succeeded)
            {
                return Created(string.Empty, string.Empty);
            }

            return Problem(userCreateResult.Errors.First().Description, null, 500);
        }



        private string GenerateJwt(AppUser user, IList<string> roles)
        {
            var claims = new List<Claim>
    {
        new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
        new Claim(ClaimTypes.Name, user.UserName),
        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
        new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
    };

            var roleClaims = roles.Select(r => new Claim(ClaimTypes.Role, r));
            claims.AddRange(roleClaims);

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(jwtSettings.ExpirationInDays));

            var token = new JwtSecurityToken(
                issuer: jwtSettings.Issuer,
                audience: jwtSettings.Issuer,
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
