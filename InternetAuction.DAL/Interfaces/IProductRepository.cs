﻿using InternetAuction.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InternetAuction.DAL.Interfaces
{
    public interface IProductRepository : IRepository<Product>
    {
            Task<IEnumerable<Product>> GetProductsByAuthorId(int id);
            Task<IEnumerable<Product>> GetProductByCategory(string categoryName);
            Task<IEnumerable<Product>> GetProductsByPublicationData(DateTime date);
            Task<IEnumerable<Product>> GetProductsByClosingData(DateTime date);
            Task<IEnumerable<Product>> GetAllWithDetails();
            Task<Product> GetByIdWithDetails(int id);
            Task<IEnumerable<Product>> GetProductsByAuthorName(string authorName);
    }
}
