﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InternetAuction.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        ICommentRepository Comments { get; }
        IOrderRepository Orders { get; }
        IProdCategoryRepository Categories { get; }
        IProductRepository Products { get; }
    }
}
