﻿using InternetAuction.DAL.Entities;
using InternetAuction.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InternetAuction.DAL.Interfaces
{
    public interface IProdCategoryRepository : IRepository<ProductCategory>
    {
        Task<IEnumerable<ProductCategory>> GetAllWithDetails();
        Task<ProductCategory> GetByIdWithDetails(int id);
    }
}
