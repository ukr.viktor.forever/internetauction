﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetAuction.DAL.Entities
{
    public class OrderType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
