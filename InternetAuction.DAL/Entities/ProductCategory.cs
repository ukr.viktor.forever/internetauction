﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetAuction.DAL.Entities
{
    public class ProductCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
