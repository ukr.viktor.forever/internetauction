﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace InternetAuction.DAL.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        [MaxLength(800)]
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public Product Product { get; set; }
        public ICollection<Comment> ChildComments { get; set; }
    }
}
