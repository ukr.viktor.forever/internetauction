﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetAuction.DAL.Entities
{
    public class Price
    {
        public int Id { get; set; }
        public double StartingPrice { get; set; }
        public double ActualPrice { get; set; }
    }
}
