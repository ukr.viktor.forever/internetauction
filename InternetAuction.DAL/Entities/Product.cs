﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetAuction.DAL.Entities
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<Picture> Pictures { get; set; }
        public DateTime PublicationDate { get; set; }
        public DateTime ClosingDate { get; set; }
        public Price Price { get; set; }
        public ProductCategory Category { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public int AuthorId { get; set; }
    }




}
