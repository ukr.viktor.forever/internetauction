﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetAuction.DAL.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public AppUser User { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public OrderType OrderType { get; set; }
        public bool IsConfirmed { get; set; }
        public Product Product { get; set; }
    }

}
