﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InternetAuction.DAL.Interfaces;
using InternetAuction.DAL.Repositories;

namespace InternetAuction.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AuctionContext Context;
        private ICommentRepository comments;
        private IOrderRepository orders;
        private IProdCategoryRepository categories;
        private IProductRepository products;
        public UnitOfWork(AuctionContext context)
        {
            Context = context;
        }
        public ICommentRepository Comments
        {
            get
            {
                if (comments == null)
                {
                    comments = new CommentRepository(Context);
                }
                return comments;
            }
        }
        public IOrderRepository Orders
        {
            get
            {
                if (orders == null)
                {
                    orders = new OrderRepository(Context);
                }
                return orders;
            }
        }
        public IProdCategoryRepository Categories
        {
            get
            {
                if (categories == null)
                {
                    categories = new ProdCategoryRepository(Context);
                }
                return categories;
            }
        }
        public IProductRepository Products
        {
            get
            {
                if (products == null)
                {
                    products = new ProductRepository(Context);
                }
                return products;
            }
        }
    }
}
