﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InternetAuction.DAL.Entities;
using InternetAuction.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace InternetAuction.DAL.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(AuctionContext context) : base(context)
        {

        }

        public async Task<IEnumerable<Order>> GetAllWithDetails()
        {
            return await Context.Orders.Include(x => x.Product).Include(x=>x.OrderType).Include(x=>x.User).ToListAsync();
        }

        public Task<Order> GetByIdWithDetails(int id)
        {
            return Context.Orders.Where(x => x.Id == id).Include(x => x.Product).Include(x => x.OrderType).Include(x => x.User).FirstOrDefaultAsync();
        }
    }
}
