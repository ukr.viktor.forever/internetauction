﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InternetAuction.DAL.Entities;
using InternetAuction.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace InternetAuction.DAL.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(AuctionContext context) : base(context)
        {

        }

        public async Task<IEnumerable<Comment>> GetAllWithDetails()
        {
            return await Context.Comments.Include(x => x.Product).ToListAsync();
        }

        public async Task<Comment> GetByIdWithDetails(int id)
        {
            return await Context.Comments.Where(x => x.Id == id).Include(x => x.Product).FirstOrDefaultAsync();
        }
    }
}
