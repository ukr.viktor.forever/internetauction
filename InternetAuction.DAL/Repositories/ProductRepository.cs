﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InternetAuction.DAL.Entities;
using InternetAuction.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace InternetAuction.DAL.Repositories
{
    public  class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(AuctionContext context) : base(context)
        {

        }

        public async Task<IEnumerable<Product>> GetAllWithDetails()
        {
            return await Context.Products
                .Include(x => x.Pictures)
                .Include(x => x.Price)
                .Include(x=>x.Category)
                .Include(x=>x.Comments)
                .ToListAsync();
        }

        public async Task<Product> GetByIdWithDetails(int id)
        {
            return await Context.Products
                .Where(x => x.Id == id)
                .Include(x => x.Pictures)
                .Include(x => x.Price)
                .Include(x => x.Category)
                .Include(x => x.Comments)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Product>> GetProductByCategory(string categoryName)
        {
            return await Context.Products
                .Where(x => x.Category.Name == categoryName)
                .Include(x => x.Pictures)
                .Include(x => x.Price)
                .Include(x => x.Category)
                .Include(x => x.Comments)
                .ToListAsync();
        }

        public async Task<IEnumerable<Product>> GetProductsByAuthorId(int id)
        {
            return await Context.Products
                .Where(x => x.AuthorId == id)
                .Include(x => x.Pictures)
                .Include(x => x.Price)
                .Include(x => x.Category)
                .Include(x => x.Comments)
                .ToListAsync();
        }


        public async Task<IEnumerable<Product>> GetProductsByPublicationData(DateTime date)
        {
            return await Context.Products
                .Where(x => x.PublicationDate == date)
                .Include(x => x.Pictures)
                .Include(x => x.Price)
                .Include(x => x.Category)
                .Include(x => x.Comments)
                .ToListAsync();
        }
        public async Task<IEnumerable<Product>> GetProductsByClosingData(DateTime date)
        {
            return await Context.Products
                .Where(x => x.ClosingDate == date)
                .Include(x => x.Pictures)
                .Include(x => x.Price)
                .Include(x => x.Category)
                .Include(x => x.Comments)
                .ToListAsync();
        }
        public async Task<IEnumerable<Product>> GetProductsByAuthorName(string authorName)
        {
            var usersIds = Context.Users
                .Where
                (
                    x => x.LastName.Contains(authorName) ||
                    x.FirstName.Contains(authorName) ||
                    (x.FirstName + x.LastName).Contains(authorName)
                )
                .Select(x =>x.Id);

            return await Context.Products
                .Where(x => usersIds.Contains(x.AuthorId))
                .Include(x => x.Pictures)
                .Include(x => x.Price)
                .Include(x => x.Category)
                .Include(x => x.Comments)
                .ToListAsync();
        }
    }
}
