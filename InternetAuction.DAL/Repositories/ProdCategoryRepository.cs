﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InternetAuction.DAL.Entities;
using InternetAuction.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace InternetAuction.DAL.Repositories
{
    public class ProdCategoryRepository : Repository<ProductCategory>, IProdCategoryRepository
    {
        public ProdCategoryRepository(AuctionContext context) : base(context)
        {

        }

        public async Task<IEnumerable<ProductCategory>> GetAllWithDetails()
        {
            return await Context.ProductCategories.Include(x => x.Products).ToListAsync();
        }

        public Task<ProductCategory> GetByIdWithDetails(int id)
        {
            return Context.ProductCategories.Where(x => x.Id == id).Include(x => x.Products).FirstOrDefaultAsync();
        }
    }
}
